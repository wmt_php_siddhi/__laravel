<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('students', 'apiController@getAllStudents');
Route::get('students/{id}', 'apiController@getStudent');
Route::post('students', 'apiController@createStudent');
Route::put('students/{id}', 'apiController@updateStudent');
Route::delete('students/{id}', 'apiController@deleteStudent');
