@extends('layouts.app')
<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Show</title>
@section('content')
    <div class="container">

        <table class="table">
            <thead>
            <tr>

                <th scope="col">Title</th>
                <th scope="col">Pages</th>
                <th scope="col">Language</th>
                <th scope="col">Book_Author</th>
                <th scope="col">Cover_img</th>
                <th scope="col">ISBN_code</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>
                <th scope="col">Delete</th>
                <th scope="col">Edit</th>
                <th scope="col">Show</th>
                <th>  <a type="btn" class="btn btn-primary" href="/book/create">add</a></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $data)
                <tr>

                    <td>{{$data->title}}</td>
                    <td>{{$data->pages}}</td>
                    <td>{{$data->language}}</td>
                    <td>{{$data->book_author}}</td>
                    <td><img src="{{url('/storage/'.$data->cover_img)}}" width="100px;" height="100px;"></td>
                    <td>{{$data->ISBN_no}}</td>
                    <td>{{$data->description}}</td>
                    <td>@if($data->status == 1)
                            <form action="{{ route('book_status', $data->id) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-success" name="status" value="0">Active</button>
                            </form>
                        @else
                            <form action="{{ route('book_status', $data->id) }}" method="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-secondary" name="status" value="1">Inactive</button>
                            </form>
                        @endif
                    </td>

                    <td>
                        <form method="post" class="delete_form" action="/book/{{$data->id}}">
                            @csrf
                            <input type="hidden" name="_method" value="DELETE" />
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>

                    <td><a type="button" class="btn btn-success" href="/book/{{$data->id}}/edit">Edit</a></td>
                    <td><a type="button" class="btn btn-success" href="/book/{{$data->id}}">show</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
