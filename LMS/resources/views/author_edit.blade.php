@extends('layouts.app')

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registraion</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@section('content')

<div class="container">

    <form action={{route('author.update',$data->id)}} method="post">
{{--        <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
        {{csrf_field()}}
        @method('PATCH')

        <div class="form-group">
            <label for="full_name">Full_Name:</label>
            <input type="text" class="form-control @error('full_name') is-invalid @enderror" id="full_name" name="full_name"  value="{{$data->full_name}}" autofocus>
            @error('full_name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="dob">Date of Birth</label>
            <input type="date" class="form-control @error('dob') is-invalid @enderror" value = "{{$data->dob}}"id="dob" name="dob">
            @error('dob')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
{{--        <div class="form-group">--}}
{{--            <label for="gender">Gender:</label>--}}
{{--            <input type="text" class="form-control @error('gender') is-invalid @enderror" id="gender"  value="{{$data->gender}}"  name="gender">--}}
{{--            @error('gender')--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--            @enderror--}}
{{--        </div>--}}

{{--</div>--}}
<div class = "form-group">
    <div id="gender-group" class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
        <label for="gender" class="col-md-4 control-label">Gender</label>

        <div class="col-md-6">
            <div><input id="female" type="radio"class="form-control" name="gender"  value="female" {{ $data->gender == 'female' ? 'checked' : ''}} >Female</div>
            <div><input id="male" type="radio"class="form-control" name="gender"  value="male" {{ $data->gender == 'male' ? 'checked' : ''}} >Male</div>

            @if ($errors->has('gender'))
                <span class="help-block">
        <strong>{{ $errors->first('gender') }}</strong>
        </span>
            @endif
        </div>
    </div>
</div>


        <div class="form-group">
            <label for="addres">Address:</label>
            <textarea class="form-control @error('address') is-invalid @enderror" rows="5" id="address"  name="address">{{$data->address}} </textarea>
            @error('address')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="ph_no">Mobile:</label>
            <input type="tel" class="form-control @error('ph_no') is-invalid @enderror" id="ph_no"   value="{{$data->ph_no}}" name="ph_no">
            @error('ph_no')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">description:</label>
            <textarea class="form-control @error('desciption') is-invalid @enderror" rows="5" id="description"    name="description">{{$data->description}}</textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
