@extends('layouts.app')

{{--    <meta charset="UTF-8">--}}
{{--    <meta name="viewport"--}}
{{--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">--}}
{{--    <meta http-equiv="X-UA-Compatible" content="ie=edge">--}}
<title>Registraion</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@section('content')


    <div class="container">

        <form action={{route('book.update',$data->id)}} method="post" enctype="multipart/form-data">
            {{--        <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
            {{csrf_field()}}
            @method('PATCH')

            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" value="{{$data->title}}" name="title" autofocus>
                @error('title')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="pages">Pages</label>
                <input type="text" class="form-control @error('pages') is-invalid @enderror"  value="{{$data->pages}}" id="pages" name="pages">
                @error('pages')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="language">Language :</label>
                <input type="text" class="form-control @error('language') is-invalid @enderror" value="{{$data->language}}"  id="language" name="language">
                @error('language')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Book Author</label>
                <select class="custom-select" name="book_author" id="book_author">
                    <option hidden>Book Author</option>
                    @foreach($author as $author)
                        <option value="{{ $author->id }}" {{ $data->book_author==$author->id?'selected':'' }}>{{ $author->full_name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="ISBN_no">Cover Image:</label>
                <input type="file" class="form-control @error('cover_img') is-invalid @enderror" id="cover_img" value="{{$data->cover_img}}" name="cover_img">
                @error('cover_img')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="ISBN_no">ISBN Code</label>
                <input type="text" class="form-control @error('ISBN_no') is-invalid @enderror"  value="{{$data->ISBN_no}}" id="ISBN_no" name="ISBN_no">
                @error('ISBN_no')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">description:</label>
                <textarea class="form-control @error('desciption') is-invalid @enderror" value="" rows="5" id="description" name="description">{{$data->description}}</textarea>
                @error('description')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>

@endsection
