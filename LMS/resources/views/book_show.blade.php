@extends('layouts.app')
<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Show</title>
@section('content')
    <div class="container">

        <table class="table">
            <thead>
            <tr>

                <th scope="col">Title</th>
                <th scope="col">Pages</th>
                <th scope="col">Language</th>
                <th scope="col">Book_Author</th>
                <th scope="col">Cover_img</th>
                <th scope="col">ISBN_code</th>
                <th scope="col">Description</th>
                <th scope="col">Status</th>

            </tr>
            </thead>
            <tbody>
            @foreach($data as $data)
                <tr>

                    <td>{{$data->title}}</td>
                    <td>{{$data->pages}}</td>
                    <td>{{$data->language}}</td>
                    <td>{{$data->book_author}}</td>
                    <td><img src="{{url('/storage/'.$data->cover_img)}}" width="100px;" height="100px;"> </td>
                    <td>{{$data->ISBN_no}}</td>
                    <td>{{$data->description}}</td>
                    <td>@if($data->status == 1)
                            {{$data->status = 'available'}}
                            @else
                                {{$data->status = 'unavailable'}}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
