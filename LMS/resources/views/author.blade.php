@extends('layouts.app')
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Show</title>
@section('content')
<div class="container">

    <table class="table">
        <thead>
        <tr>

            <th scope="col">FullName</th>
            <th scope="col">Date_of_Birth</th>
            <th scope="col">Gender</th>
            <th scope="col">Address</th>
            <th scope="col">Phone_no.</th>
            <th scope="col">Description</th>
            <th scope="col">Status</th>
            <th scope="col">Delete</th>
            <th scope="col">Edit</th>
            <th scope="col">show</th>
            <th>  <a type="btn" class="btn btn-primary" href="/author/create">add</a></th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $data)
            <tr>

                <td>{{$data->full_name}}</td>
                <td>{{$data->dob}}</td>
                <td>{{$data->gender}}</td>
                <td>{{$data->address}}</td>
                <td>{{$data->ph_no}}</td>
                <td>{{$data->description}}</td>
                <td>@if($data->status == 1)
                        <form action="{{ route('status', $data->id) }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-success" name="status" value="0">Active</button>
                        </form>
                    @else
                        <form action="{{ route('status', $data->id) }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-secondary" name="status" value="1">Inactive</button>
                        </form>
                    @endif
                </td>
                <td>
                    <form method="post" class="delete_form" action="/author/{{$data->id}}">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE" />
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>

                <td><a type="button" class="btn btn-success" href="/author/{{$data->id}}/edit">Edit</a></td>
                <td><a type="button" class="btn btn-success" href="/author/{{$data->id}}">show</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
