<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Book;
class Authors extends Model
{
    public function books(){
        return $this->hasMany(Book::class);
    }
}
