<?php

namespace App\Http\Controllers;

use App\Authors;
use App\Book;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\Auth::check()) {
            $data = Authors::all();
            return view('author', compact('data'));
        } else {
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auhtor_add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'full_name' => ['required', 'max:50', 'alpha'],
            'dob' => ['required'],
            'gender' => ['required'],
            'address' => ['required', 'max:255'],
            'ph_no' => ['required', 'numeric', 'digits:10'],
            'description' => ['required'],
        ]);

        $data = new Authors();
        $data->full_name = $request->full_name;
        $data->dob = $request->dob;
        $data->gender = $request->gender;
        $data->address = $request->address;
        $data->ph_no = $request->ph_no;
        $data->description = $request->description;
        $data->save();

        return redirect()->route('author.index');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Authors::all();
        return view('author_show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Authors::find($id);
        return view('author_edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'full_name' => ['required', 'max:50', 'string'],
            'dob' => ['required'],
            'gender' => ['required','alpha'],
            'address' => ['required', 'max:255','string'],
            'ph_no' => ['required', 'numeric', 'digits:10'],
            'description' => ['required'],
        ]);

        $data = Authors::find($id);
        $data->full_name = $request->get('full_name');
        $data->dob = $request->get('dob');
        $data->gender = $request->get('gender');
        $data->address = $request->get('address');
        $data->ph_no = $request->get('ph_no');
        $data->description = $request->get('description');

        $data->save();
        return redirect()->route('author.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Authors::find($id);
        $data->delete();
        return redirect()->route('author.index')->with('success', 'Data Deleted');
    }

    public function status($id)
    {
        $data = Authors::find($id);
        if($data->status == 1){
            $data->status = 0;
        }else{
            $data->status = 1;
        }
        $data->save();
        return redirect()->route('author.index');
    }
}
