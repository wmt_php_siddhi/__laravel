<?php

namespace App\Http\Controllers;


use App\Authors;
use App\Book;
use Illuminate\Http\Request;



class bookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::check()) {
            $data = Book::all();
            return view('book', compact('data'));
        }
        else{
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $author = Authors::all();
        return view('book_add',compact('author'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd('dfh');
        $request->validate([
            'title' => ['required','alpha_dash'],
            'pages' => ['required'],
            'language'=>['required'],
            'book_author' => ['required','string'],
            'cover_img' => ['required'],
            'ISBN_no'=>['required','unique:books'],
            'description'=>['required'],
        ]);
//    dd('hi');

        $data = new Book();
        $data->title = $request->title;
        $data->pages = $request->pages;
        $data->language = $request->language;
        $data->book_author = $request->book_author;



//        $file = $request->file('cover_img');
//        $filename = '' . time() . '.' . $file->getClientOriginalExtension();
//        $data = $file->storeAs('storage/app/images', $filename);




//            $filename = $request->file('cover_img');
//            $extension = $request->file('cover_img')->getClientOriginalExtension();
//            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
//            $path = $filename->storeAs('images', $fileNameToStore,'public');

            $image = $request->file('cover_img');
            $originalname = uniqid('Img',10) .'.'.$image->getClientOriginalExtension();
            $path = $image->storeAs('/images/userimagepost',$originalname,'public');
        $data->cover_img = $path;
        $data->ISBN_no = $request->ISBN_no;
        $data->description = $request->description;

        $data->save();

        return redirect()->route('book.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = Book::all();
        return view('book_show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Book::find($id);
        $author = Authors::all();
        return view('book_update', compact('data','author'));
//

//        return view('book_update',compact('author'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Book::find($id);
        $request->validate([
            'title' => ['required','alpha_dash'],
            'pages' => ['required'],
            'language'=>['required'],
            'book_author' => ['required','string'],
            'cover_img' => ['required'],
            'ISBN_no'=>['required','unique:books,ISBN_no,'.$data->id],
            'description'=>['required'],
        ]);


        $data->title = $request->title;
        $data->pages = $request->pages;
        $data->language = $request->language;
        $data->book_author = $request->book_author;




        $image = $request->file('cover_img');
        $originalname = uniqid('Img',10) .'.'.$image->getClientOriginalExtension();
        $path = $image->storeAs('/images/userimagepost',$originalname,'public');
        $data->cover_img = $path;
        $data->ISBN_no = $request->ISBN_no;
        $data->description = $request->description;
        $data->save();
        return redirect()->route('book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Book::find($id);
        $data->delete();
        return redirect()->route('book.index')->with('success', 'Data Deleted');
    }
    public function status($id)
    {
        $data = Book::find($id);
        if($data->status == 1){
            $data->status = 0;
        }else{
            $data->status = 1;
        }
        $data->save();
        return redirect()->route('book.index');
    }
}
