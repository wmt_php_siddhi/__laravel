<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Authors;

class Book extends Model
{
    public function authors(){
        return $this->belongsTo(Authors::class,'book_author','id');
    }
    protected $guarded = [];
    const UPDATED_AT = null;
    const CREATED_AT = null;
    public function setUpdatedAt($value)
    {
        return $this;
    }
    public function setCreatedAtdAt($value)
    {
        return $value;
    }

            public function getCoverImgAttributes($value){

                return url('/storage/'.$value);
            }

}
