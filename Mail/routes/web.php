<?php
use App\Jobs\SendEmailJob;
use App\Jobs\SendMailChain;
use Carbon\Carbon;
//use  Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('SendEmail',function ()
{
//        dispatch(new SendMailChain());

    $job = (new SendEmailJob());
   dispatch($job )->delay(Carbon::now()->addSeconds(5));

//    $jobs = withChain([new SendEmailJob,new SendMailChain]);
//    dispatch($jobs);
//
//SendEmailJob::withChain(new SendMailChain() )->dispatch();
    SendEmailJob::withChain([
        new SendMailChain(),
    ])->dispatch();

    return   'Email Sent Successfully';
});
