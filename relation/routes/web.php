<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/update/{id}', 'PolyController@update');
Route::get('/add_img/{id}', 'PolyController@add_img');
Route::get('/add_video/{id}', 'PolyController@add_video');
Route::get('/country_post/{id}', 'PolyController@country_post');
Route::get('/post/{id}','PolyController@PostComment');
Route::get('/video/{id}','PolyController@videoComment');
Route::get('/postget','PolyController@getpost');
Route::get('/videoget','PolyController@getvideo');
