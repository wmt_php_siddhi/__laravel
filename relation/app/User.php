<?php

namespace App;
use App\Country;
use App\Post;
use App\Video;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function country()
    {
       return  $this->belongsTo(Country::class,'c_id','id');
    }

    public function posts()
    {
       return $this->hasMany(Post::class);
    }
    public function video()
    {
        return $this->hasMany(Video::class);
    }
}
