<?php

namespace App;
use App\User;
use App\Comment;
use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function users()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
