<?php

namespace App\Http\Controllers;
use App\Country;
use App\Post;
use App\User;
use App\Video;
use App\Comment;
use Illuminate\Http\Request;

class PolyController extends Controller
{
    public function update($id)
    {
        $user= User::find($id);
        $user->Profile_pic="new pic";
        $user->save();
        return User::find($id);
    }
    public function add_img($id)
    {
        $users = User::find($id);
        $post = new Post;
        $post->url = "girl's  img";
        $users = $users->posts()->save($post);
        return $users;
    }
    public function add_video($id)
    {
        $users = User::find($id);
        $video = new Video;
        $video->url = "girl's  img";
        $users = $users->video()->save($video);
        return $users ;
    }
    public function country_post($id)
    {
        $country = Country::find($id);
        return $country->posts;
    }
    public function PostComment($id)
    {
        $post = Post::find($id);
        $comment = new Comment;
        $comment->body = "Hi ItSolutionStuff.com";
       return  $post->comments()->save($comment);
    }
    public function videoComment($id)
    {
        $video = Video::find($id);
        $comment = new Comment;
        $comment->body = "Hi ItSolutionStuff.com";
        return $video->comments()->save($comment);
    }
    public function getpost()
    {
//        $post = Post::all();
//       return $post->comments;
        return Post::with('comments')->get();
    }
    public function getvideo()
    {
//        $video = Video::all();
//        $comment = $video->comments;
//        return $comment;
        return Video::with('comments')->get();
    }
}
