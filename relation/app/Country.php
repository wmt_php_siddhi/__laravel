<?php

namespace App;
use App\User;
use App\Post;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
        public function user()
        {
            $this->hasMany(User::class);
        }
        public function posts()
        {
            return $this->hasManyThrough(Post::class,User::class,'c_id','user_id','id','id');
        }
}
