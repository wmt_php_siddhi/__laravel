<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Detail;
class detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Detail::all();
        return view('crud.show', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('crud/user_reg');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => ['required','min:3','max:15','alpha_num'],
            'email' => ['required', 'string', 'email', 'max:255','unique:details'],
            'mobile'=>['required','digits:10','numeric','unique:details'],
            'password' => ['required', 'alpha_dash', 'min:8'],
        ]);

        $data = new Detail();
        $data->username= $request->username;
        $data->email= $request->email;
        $data->mobile= $request->mobile;
        $data->password= $request->password;
        $data->save();
        return redirect()->route('detail.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Detail::find($id);
        return view('crud.update', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = Detail::find($id);
        $data->username = $request->get('username');
        $data->email = $request->get('email');
        $data->mobile = $request->get('mobile');

        $data->save();
        return redirect()->route('detail.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $data = Detail::find($id);
        $data->delete();
        return redirect()->route('detail.index')->with('success', 'Data Deleted');

    }

}
