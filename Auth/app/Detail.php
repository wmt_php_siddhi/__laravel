<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $guarded = [];
    const UPDATED_AT = null;
    const CREATED_AT = null;
    public function setUpdatedAt($value)
    {
        return $this;
    }
    public function setCreatedAtdAt($value)
    {
        return $value;
    }
}
