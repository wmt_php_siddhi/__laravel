<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Show</title>
</head>
<body>

<div class="container">
    <a type="btn" class="btn btn-primary" href="/detail/create">add</a>
    <table class="table">
        <thead>
        <tr>

            <th scope="col">UserName</th>
            <th scope="col">Email</th>
            <th scope="col">MobileNo.</th>
            <th scope="col">Delete</th>
            <th scope="col">Edit</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $data)
            <tr>

                <td>{{$data->username}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->mobile}}</td>
                <td>
                    <form method="post" class="delete_form" action="/detail/{{$data->id}}">
                        @csrf
                        <input type="hidden" name="_method" value="DELETE" />
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                   </td>

                <td><a type="button" class="btn btn-success" href="/detail/{{$data->id}}/edit">Edit</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

</body>
</html>
