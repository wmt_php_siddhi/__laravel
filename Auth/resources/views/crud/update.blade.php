<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registraion</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>


{{--    <input type="hidden" name="_token" value="{{csrf_token()}}">--}}
    <form method="post" action={{route('detail.update',$data->id)}}>
        {{csrf_field()}}
       @method('PATCH')

    <div class="form-group">
        <label for="exampleInputEmail1">User Name</label>
        <input type="text" class="form-control"  value="{{$data->username}}" placeholder="Enter email">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input type="email" class="form-control"  value="{{$data->email}}"  placeholder="Enter email">

    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Mobile No.</label>
        <input type="tel" class="form-control"  value="{{$data->mobile}}"  placeholder="Password">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

</body>
</html>
