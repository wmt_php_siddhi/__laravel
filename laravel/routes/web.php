<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    $USER = \App\User::where('id', 1);
//    return view('welcome' );
//});
//
////through controller
//Route::get('/first{', 'firstController@index');
//
////returning data
//Route::get('/basic',function (){
//    return ' Basic route Data Returning';
//});
//
////redirecting
//Route::redirect('/demo','first');
//
////from view
//Route::get('/from_view/{user}',function ($user)
//{
//   return view('test.first',['username'=>$user]);
//});
//
////Parameters
//
//Route::get('/user/{id}',function($id){
//   return 'user id is:'.$id;
//});
//
//Route::get('username/{name}/{id}',function ($name,$id){
//   return $name . '   welcome you created Parameterized route'.$id;
//});
//
////optional para
//Route::get('/option/{name?}', function ($name = 'siddhi') {
//    return $name;
//});
//
////reg expression
//Route::get('/profile/{name}', function($name) {
//    return 'Welcome   '.$name.'  you are using Reg Exp Constraints';
//})->where('name', '[A-Za-z]+');
//
//Route::get('/id/{id}', function($id) {
//    return $id;
//})->where('id', '[0-9]+');
//
////slashes
//Route::get('search/{search}', function ($search) {
//    return $search;
//})->where('search', '.*');
//
////Naming
//Route::get('user/{id}/profile', function ($id) {
//    //
//})->name('profile');
//
//
///*Route::get('/employee', function () {
//    $emp = App\Employee::orderBy('e_name', 'desc')
//        ->take(4)
//        ->get();
//    echo $emp;
//});
//
///*Route::get('/employee', function (){
//
//    $data = App\Employee::all();
//    foreach ($data as $data) {
//        echo $data->e_name .'   ';
//    }
//    //echo $data;
//});*/
///*Route::get('/employee', function () {
//    $data = App\Employee::where('phone_no', '9989899654')->first();
//
//    $freshdata = $data->fresh();
//    echo $freshdata;
//});*/
//
//Route::get('/e_except',function (){
//    $data = App\Employee::all()->except([3,4]);
//   echo $data;
//});
//
//Route::get('/e_find',function (){
//    $data = App\Employee::all()->find([3,4]);
//    echo $data;
//});
//
//Route::get('/e_chunk',function (){
//
//    $data = \App\Employee::chunk(200, function ($data) {
//        foreach ($data as $data) {
//            echo $data;
//        }
//    });
//
//});
//
//Route::get('/e_cursor',function (){
//    $user = App\Employee::cursor()->filter(function ($user) {
//        return $user->id > 3;
//        echo $user;
//    });
//    echo $user;
//
//});
//
//Route::get('/select', 'empController@selectEmp');
//Route::get('/update','empController@updateEmp');
//Route::get('/delete','empController@deleteEmp');
//Route::get('/insert','empController@insertEmp');
//
//Route::get('/e_findfail',function (){
//    $model = App\Employee::findOrFail(7);
//});

Route::get('/extra', 'EmployeeController@one');
Route::resource('employee', 'EmployeeController');

//Route::get('/', 'WelcomeController@view');
//
//Route::get('employee/{id}', 'showProfile@employee');
//
//Route::get('department/{id}', 'showProfile@department');
