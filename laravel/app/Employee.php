<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Employee extends Model
{

    protected $fillable = [
        'e_name', 'email', 'phone_no',
    ];

    public function depart()
    {
        return $this->hasOne(department::class,'emp_id','id');
    }
}
