<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "<h4>Index</h4>";
        $employee = Employee::all();
        echo "<table border='0'>";
        echo"<tr>";
        echo"<th>ID</th>";
        echo "<th>Name</th>";
        echo "<th>Email</th>";
        echo "<th>Phone_no</th>";
        echo "</tr>";

        foreach ($employee as $data) {
            echo "<tr>";
            echo "<td>$data->id</td>";
            echo "<td>$data->e_name</td>";
            echo "<td>$data->email</td>";
            echo "<td>$data->phone_no</td>";
            echo "</tr>";
        }
        echo "</table>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "<h3>CREATE</h3>";
        echo '<form action="/employee" method="POST">';
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="text" name="e_name">';
        echo '<input type="email" name="email">';
        echo '<input type="tel" name="phone_no">';
        echo '<input type="submit">';
        echo '</form >';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        echo "<h3>STORE</h3>";
        Employee::create($request->all());
        return redirect()->route('employee.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        echo "<h3>SHOW</h3>";
        echo "<table border='1'>";
        echo "<tr>";
        echo "<th>Name</th>";
        echo "<th>email</th>";
        echo "<th>phone</th>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>$employee->e_name</td>";
        echo "<td>$employee->email</td>";
        echo"<td>$employee->phone_no</td>";
        echo "</tr>";
        echo "</table>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Employee $employee
     * @return \Illuminate\Http\Response
     */

    public function edit(Employee $employee)
    {
        echo "<h3>EDIT: $employee->e_name</h3>";
        echo "<form action='/employee/$employee->id' method='POST'>";
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="hidden" name="_method" value="PUT">';
        echo "<input type='text' name='e_name' value='$employee->e_name'>";
        echo "<input type='email' name='email' value='$employee->email'>";
        echo "<input type='tel' name='phone_no' value='$employee->phone_no'>";
        echo '<input type="submit">';
        echo '</form >';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   App\Employee $employee
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Employee $employee)
    {
//        dd($request->all());
        $employee->update($request->all());
        return redirect()->route('employee.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function one(){
        $student = Employee::find(6);
        dd ($student->depart);

    }
}
