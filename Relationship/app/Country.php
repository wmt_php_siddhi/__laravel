<?php

namespace App;
use App\Netizen;
use App\Status;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function status(){
        return $this->hasManyThrough(Status::class,Netizen::class,'country_id','n_id');
    }
}
