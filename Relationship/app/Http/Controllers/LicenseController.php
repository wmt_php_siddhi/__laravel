<?php

namespace App\Http\Controllers;

use App\Person;
use App\License;
use Illuminate\Http\Request;

class LicenseController extends Controller
{
    public function show($id){
        $person = Person::find($id);
        echo $person->licenses;
    }
}
