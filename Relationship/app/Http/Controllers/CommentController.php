<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function show(){
        $comment = Comment::find(3);

        $post = $comment->post;

        dd($post);

    }
    public function show1(){
        $post = Post::find(3);

        $comment = $post->comments;
    }


    public function com(){
//        $comment = new Comment(['comment' => 'A new comment.']);
//
//        $post = Post::find(1);
//
//        $post->comments()->save($comment);

        $post = Post::find(3);

        $post->comments()->saveMany([
            new Comment(['comment' => 'A new comment.']),
            new Comment(['comment' => 'Another comment.']),
        ]);
    }
    public function push(){
        $post = Post::find(1);

        $post->comments[0]->comment = 'Message';
        $post->comments[0]->post->name = 'Author Name';

        $post->push();
    }
}
