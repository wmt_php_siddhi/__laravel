<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Collection;
use function foo\func;

class CollectionController extends Controller
{
    public function example(){
//          return collect([1,2,3,4,5])->all();

//       return collect([ collect([1,2,3,4,5]),
 //          collect([1,2,3,4,5]),
 //                ])->toArray();

//        $avg = collect([12,23,1,45])->avg();
//        echo $avg;

//        $collection = collect([1, 2, 3, 4, 5, 6, 7]);
//        $chunks = $collection->chunk(2);
//        echo $chunks;

//        $collection = collect([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
//       echo $collection->collapse();

//        $collection = collect(['name', 'age']);
//        echo $collection->combine(['George', 29]);

//        $collectionA = collect([1, 2, 3]);
//        $collectionB = $collectionA->collect();
//        echo $collectionB;
//
//        $collection = collect([
//            ['product' => 'Desk', 'price' => 200],
//            ['product' => 'Chair', 'price' => 100],
//        ]);
//    echo   $collection->contains('product', 'Chair');

//        $collection = collect([1, 2]);
//        $matrix = $collection->crossJoin(['a', 'b'], ['I', 'II']);
//        print_r($matrix->all());

//            return collect([
//                ['banana',45,'porbandar'],
//                ['apples',23,'florida'],
//            ])->each(function ($val)
//            {
//                dump("we have {$val[1]} {$val[0]} in our {$val[2]} store");
//            });
//
//        return collect([
//            ['banana',45,'porbandar'],
//            ['apples',23,'florida'],
//        ])->eachSpread(function ($product ,$qty,$loc)
//        {
//            dump("we have {$qty} {$product} in our {$loc} store");
//        });

//        $collection = collect(['product_id'=>1,"price"=>100,"dicount"=>false]);
//        $filter = $collection->except(['price']);
//        dump($filter->all());

        $collection = collect([
            ['name'=>'siddhi'],
            ['school'=>'saint Mary,s'],
        ]);
        $flat = $collection->flatMap(function($val){
            return array_map('strtoupper',$val);

        });
        dump($flat->all());
    }
}
