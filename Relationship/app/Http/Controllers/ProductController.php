<?php

namespace App\Http\Controllers;
use App\Category;
use App\Product;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function create(Request $request, Product $id)
    {
        $product = new Product;
        $product->name = 'Zudio';
        $product->price = 450;

        $product->save();

        $category = Category::find($id);
        $product->categories()->attach($category);

        return 'Success';
    }

    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }
    public function removeCategory(Product $product)
    {
        $category = Category::find(3)->roles()->orderBy('title');

        $product->categories()->detach($category);

        return 'Success';
    }
}
