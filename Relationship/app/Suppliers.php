<?php

namespace App;

use App\Goods;
use App\Orders;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    public function orders(){

        return $this->hasOneThrough(Orders::class,Goods::class,'supplier_id','goods_id','id','id');

    }
}
