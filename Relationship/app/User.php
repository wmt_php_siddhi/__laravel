<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function getNameAttribute($val){
        return ucfirst($val);

    }
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }
}
