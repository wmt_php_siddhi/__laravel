<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    public function licenses()
    {
        return $this->hasOne(License::class, 'id', 'pid');
    }
}
