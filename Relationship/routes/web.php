<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('product/create/{id}', 'ProductController@create')->name('product.create');
Route::get('product/{product}', 'ProductController@show')->name('product.show');
Route::get('category/product/{product}', 'ProductController@removeCategory')->name('category.product.delete');
Route::get('comment/show', 'CommentController@show')->name('comment.show');
Route::get('show1', 'CommentController@show1');
Route::get('person/show/{id}', 'LicenseController@show');

Route::get('com', 'CommentController@com');
Route::get('push', 'CommentController@push');
Route::get('hasone/{id}','OneController@hasone');
Route::get('hasmany/{id}','OneController@hasmany');


//Accessor

Route::get('accessor/{id}','TestController@accessor');
Route::get('mutetor/{id}','TestController@mutetor');

//collection
Route::get('example','CollectionController@example');
